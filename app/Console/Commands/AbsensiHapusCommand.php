<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use  App\Models\Absensi;
use App\Models\AbsensiStaff;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel\Month;

class AbsensiHapusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:hapus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $absen_siswa = Absensi::get();
        $absen_staff = AbsensiStaff::get();
        $now = Carbon::now()->format('Y-m-d');

        foreach ($absen_staff as $absenstaff) {
            if (Carbon::parse($absenstaff->created_at)->addMonth(7)->format('Y-m-d') == $now) {
                $absenstaff->delete();
            }
        }

        foreach ($absen_siswa as $absensiswa) {
            if (Carbon::parse($absensiswa->created_at)->addMonth(7)->format('Y-m-d') == $now) {
                $absensiswa->delete();
            }
        }
    }
}
