<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Kelas;
use App\Models\Pesan;
use App\Models\Siswa;
use App\Models\Absensi;
use App\Models\SecretKey;
use Illuminate\Support\Str;
use App\Models\AbsensiStaff;
use Illuminate\Http\Request;
use App\Models\WaktuOperasional;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index()
    {
        $totalUser = User::where('id', '!=', 1)->where('is_active', 1)->count();
        $totalSiswa = Siswa::where('is_active', 1)->count();
        $totalKelas = Kelas::count();
        $totalPengguna = $totalUser + $totalSiswa;

        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d 00:00:00');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d 00:00:00');

        $hadirStaff = AbsensiStaff::where('status_hadir', 'Hadir')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $zoomStaff = AbsensiStaff::where('status_hadir', 'Hadir Via Zoom')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $sakitStaff = AbsensiStaff::where('status_hadir', 'Sakit')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $ijinStaff = AbsensiStaff::where('status_hadir', 'Ijin')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $alpaStaff = AbsensiStaff::where('status_hadir', 'Alpa')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();

        $hadirSiswa = Absensi::where('status_hadir', 'Hadir')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $zoomSiswa = Absensi::where('status_hadir', 'Hadir Via Zoom')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();

        $sakitSiswa = Absensi::where('status_hadir', 'Sakit')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $ijinSiswa = Absensi::where('status_hadir', 'Ijin')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $alpaSiswa = Absensi::where('status_hadir', 'Alpa')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();

        $siswa = Absensi::where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->where('masuk', 0)->where('keluar', 0)->get();

        return view('dashboard.index', compact('totalUser', 'totalSiswa', 'totalKelas', 'totalPengguna', 'hadirStaff', 'zoomStaff', 'sakitStaff', 'ijinStaff', 'alpaStaff', 'hadirSiswa', 'zoomSiswa', 'sakitSiswa', 'ijinSiswa', 'alpaSiswa', 'siswa'));
    }

    public function setting()
    {
        auth()->user()->can('setting-access') ? true : abort(403);

        $secretKey = SecretKey::find(1);

        $waktu = WaktuOperasional::find(1);
        $weekday = WaktuOperasional::find(2);
        $friday = WaktuOperasional::find(3);
        $ijinPulang = WaktuOperasional::find(4);

        $masuk = explode(' - ', $waktu->waktu_masuk);
        $keluar = explode(' - ', $waktu->waktu_keluar);
        $masukWeekday = explode(' - ', $weekday->waktu_masuk);
        $keluarWeekday = explode(' - ', $weekday->waktu_keluar);
        $masukFri = explode(' - ', $friday->waktu_masuk);
        $keluarFri = explode(' - ', $friday->waktu_keluar);

        $pesan = SecretKey::find(2);
        return view('dashboard.setting', compact('waktu', 'weekday', 'friday', 'ijinPulang', 'masuk', 'keluar', 'masukWeekday', 'keluarWeekday', 'masukFri', 'keluarFri', 'secretKey', 'pesan'));
    }

    public function updateWaktu(Request $request, $id)
    {

        $request->validate([
            'waktu_awal_masuk' => 'required',
            'waktu_akhir_masuk' => 'required',
            'waktu_awal_keluar' => 'required',
            'waktu_akhir_keluar' => 'required',
            'waktu_awal_masuk_week' => 'required',
            'waktu_akhir_masuk_week' => 'required',
            'waktu_awal_keluar_week' => 'required',
            'waktu_akhir_keluar_week' => 'required',
            'waktu_awal_masuk_fri' => 'required',
            'waktu_akhir_masuk_fri' => 'required',
            'waktu_awal_keluar_fri' => 'required',
            'waktu_akhir_keluar_fri' => 'required',
            'telat_siswa' => 'required',
            'telat_staff' => 'required',
            'telat_fri' => 'required',
            'waktu_awal_ijin' => 'required',
            'waktu_akhir_ijin' => 'required',
        ]);

        try {
            DB::beginTransaction();
            // Waktu Masuk Siswa
            $waktuOperasional = WaktuOperasional::find($id);

            $masuk = $request->waktu_awal_masuk . ' - ' . $request->waktu_akhir_masuk;
            $keluar = $request->waktu_awal_keluar . ' - ' . $request->waktu_akhir_keluar;

            $waktuOperasional->update([
                'waktu_masuk' => $masuk,
                'waktu_keluar' => $keluar,
                'telat' => $request->telat_siswa
            ]);

            // Waktu Masuk Staff (Mon - Thu)
            $weekday = WaktuOperasional::find(2);

            $masukWeek = $request->waktu_awal_masuk_week . ' - ' . $request->waktu_akhir_masuk_week;
            $keluarWeek = $request->waktu_awal_keluar_week . ' - ' . $request->waktu_akhir_keluar_week;

            $weekday->update([
                'waktu_masuk' => $masukWeek,
                'waktu_keluar' => $keluarWeek,
                'telat' => $request->telat_staff
            ]);

            // Waktu Masuk Friday
            $friday = WaktuOperasional::find(3);

            $masukFri = $request->waktu_awal_masuk_fri . ' - ' . $request->waktu_akhir_masuk_fri;
            $keluarFri = $request->waktu_awal_keluar_fri . ' - ' . $request->waktu_akhir_keluar_fri;

            $friday->update([
                'waktu_masuk' => $masukFri,
                'waktu_keluar' => $keluarFri,
                'telat' => $request->telat_fri
            ]);

            // Waktu ijin Pulang
            $ijin = WaktuOperasional::find(4);

            $ijin->update([
                'waktu_masuk' => $request->waktu_awal_ijin,
                'waktu_keluar' => $request->waktu_akhir_ijin,
            ]);

            DB::commit();

            return redirect()->route('setting')->with('success', 'Waktu Operasional berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('setting')->with('error', $th->getMessage());
        }
    }

    public function profile()
    {
        return view('dashboard.profile');
    }

    public function update(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'gender' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $user = User::find(auth()->user()->id);

            if ($request->password) {
                $password = bcrypt($request->password);
            } else {
                $password = auth()->user()->password;
            }

            if ($request->file('foto')) {
                Storage::delete($user->foto);
                $foto = $request->file('foto');
                $fotoUrl = $foto->storeAs('images/user', date('YmdHis') . '-' . Str::slug($request->nama) . '.' . $foto->extension());
            } else {
                $fotoUrl = $user->foto;
            }


            $user->update([
                'nama' => $request->nama,
                'gender' => $request->gender,
                'password' => $password,
                'foto' => $fotoUrl,
            ]);

            DB::commit();
            return back()->with('success', 'Profile berhasil diupdate');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function updatePesan(Request $request, $id)
    {
        // $siswa = Siswa::where('rfid', 123)->first();
        // dd($siswa->kelas->nama);

        // $no_hp_ortu = str_split($siswa->no_hp_ortu);
        // if (str_contains($siswa->no_hp_ortu, '08')) {
        //     unset($no_hp_ortu[0]);
        //     array_unshift($no_hp_ortu, '62');
        // }
        // $no_hp_ortu = implode("", $no_hp_ortu);
       
        // $pesans = explode(' ', $request->pesan);
        // $text = '';

        // foreach ($pesans as $pesan) {
        //     if (str_contains($pesan, '$')) {
        //         if ($pesan == '$waktu') {
        //             $text .= ' ' . Carbon::now() . ' ';
        //         } else {
        //             $req = Siswa::where('rfid', 123)->select(ltrim($pesan, '$'))->first()->toArray()[ltrim($pesan, '$')];
        //             $text .= $req;
        //         }
        //     } else {
        //         $text .= ' ' . $pesan;
        //     }
        // }

        try {
            DB::beginTransaction();
            $pesan = SecretKey::find($id);

            $pesan->update([
                'key' => $request->pesan,
            ]);

            DB::commit();
            return back()->with('success', 'Pesan berhasil diupdate');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
